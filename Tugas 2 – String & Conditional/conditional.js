//Soal 1

var nama = "ibnu"
var peran = "Guard"


if ( nama == "" ) {
    console.log("Nama harus diisi!")
} else if ( peran == "" ) {
    console.log("Halo " + nama +", Pilih peranmu untuk memulai game!")
} else if ( peran == "Penyihir" ){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran +" " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if ( peran == "Guard" ){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran +" " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if ( peran == "Werewolf" ){
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log("Halo " + peran +" " + nama + ", Kamu akan memakan mangsa setiap malam!")
}


//Soal 2
var tanggal = 39; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 19; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 19994; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var strBulan;
var salah = false;

if ( tanggal < 1 || tanggal > 31 ) {
    console.log("Input tanggal anda salah!"); 
    salah = true;
}
switch(bulan) {
    case 1:   { strBulan = "Januari"; break; }
    case 2:   { strBulan = "Februari"; break; }
    case 3:   { strBulan = "Maret"; break; }
    case 4:   { strBulan = "April"; break; }
    case 5:   { strBulan = "Mei"; break; }
    case 6:   { strBulan = "Juni"; break; }
    case 7:   { strBulan = "Juli"; break; }
    case 8:   { strBulan = "Agustus"; break; }
    case 9:   { strBulan = "September"; break; }
    case 10:   { strBulan = "Oktober"; break; }
    case 11:   { strBulan = "November"; break; }
    case 12:   { strBulan = "Desember"; break; }
    default:  { console.log("Input Bulan anda salah!"); salah = true;}
}
if ( tahun < 1900 || tahun > 2200 ) {
    console.log("Input tahun anda salah!"); 
    salah = true;
}

if ( salah == true ) {
    console.log("Input data anda salah! Input data lain!")
} else {
    console.log(tanggal + " " + strBulan + " " + tahun)
}

