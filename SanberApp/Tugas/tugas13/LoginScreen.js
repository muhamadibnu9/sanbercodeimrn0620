import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, TextInput, FlatList, Text, View, Image, TouchableOpacity, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from './images/logo.png';

export default function LoginScreen() {
    return (
      <View style={styles.container}>
          
            <Image  style={{width:375, height: 116, marginBottom: 30}} source={require('./images/logo.png')}/>    
            <Text style={{alignContent: "center", fontSize: 24, marginBottom: 30, fontWeight: "bold"}}>Masuk</Text>
           
        <View style={{alignContent:"center"}}>
             <Text style={{alignContent: "flex-start"}}>Email</Text>
            <TextInput style={styles.inputBox}/>
            <Text style={styles.title}>Password</Text>
            <TextInput style={styles.inputBox}/>
        </View>
            <TouchableOpacity style={styles.buttond}>
                <Text style={styles.buttonText}>Daftar</Text>
            </TouchableOpacity>
            <Text>atau</Text>
            <TouchableOpacity style={styles.buttonm}>
                <Text style={styles.buttonText}>Masuk</Text>
            </TouchableOpacity>
          
          
      </View>
);}

const styles = StyleSheet.create({
    container:{
        flexGrow: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    buttonm: {

        width:100,
        backgroundColor:'#3EC6FF',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
        },
    buttond: {
        marginTop:50,
        width:100,
         backgroundColor:'#20232a',
         borderRadius: 25,
         marginVertical: 10,
         paddingVertical: 13
         },
    inputBox: {

        width:300,
        height:35,
        borderColor: "#20232a",
        backgroundColor: "#EFEFEF"
    },
    buttonText: {
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign:'center'
        }
});