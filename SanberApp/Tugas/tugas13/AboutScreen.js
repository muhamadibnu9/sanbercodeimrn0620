import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, TextInput, FlatList, Text, View, Image, TouchableOpacity, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionic from 'react-native-vector-icons/Ionicons';
import logo from './images/logo.png';

export default function LoginScreen() {
    return (
      <View style={styles.container}>
        <Text style={{alignContent: "center", fontSize: 24, paddingTop:40, marginBottom: 10, fontWeight: "bold"}}>Tentang Saya</Text>
        <Icon style={styles.navItem} name="account-circle" size={250}/>   
        <Text style={{alignContent: "center", fontSize: 20, marginBottom: 10, fontWeight: "bold"}}>Muhamad Ibnu Qoyim</Text>
        <View style={styles.boxh}>
        <Text style={styles.title}>PORTOFOLIO</Text>
        <View style={{height:0.5, alignSelf:"center", width:290,backgroundColor:'black'}}/>
            <View style={styles.portofolio}>
            <View>
                <Ionic name="logo-github" size={50}/>
                <Text style={{alignSelf:"center"}}>ibnuqoyim</Text>
            </View>
            
            </View>
        </View>
        
        <View style={styles.boxv}>
            <Text style={styles.title}>KONTAK</Text>
            <View style={{height:0.5, alignSelf:"center", width:290,backgroundColor:'black'}}/>
            <View style={styles.contact}>
            <View style={{flexDirection:"row"}}>
                <Ionic name="logo-facebook" size={50}/>
                <Text style={styles.textItem}>Muhamad Ibnu Qoyim</Text>
            </View>
            <View style={{flexDirection:"row"}}>
                <Ionic name="logo-instagram" size={50}/>
                <Text style={styles.textItem}>@ibnuq</Text>
            </View>
            <View style={{flexDirection:"row"}}>
                <Ionic name="logo-twitter" size={50}/>
                <Text style={styles.textItem}>@ibnuqoyim</Text>
            </View>
            </View>
        </View>
          
          
      </View>
);}

const styles = StyleSheet.create({
    container:{
        flexGrow: 1,
        justifyContent:'center',
        alignItems: 'center'
    },
    buttonm: {

        width:100,
        backgroundColor:'#3EC6FF',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
        },
    buttond: {
        marginTop:50,
        width:100,
         backgroundColor:'#20232a',
         borderRadius: 25,
         marginVertical: 10,
         paddingVertical: 13
         },
    boxh:{
        borderRadius: 25,
        marginVertical: 10,
        width:300,
        height:150,
        backgroundColor: "#EFEFEF"
    },
    boxv:{
        borderRadius: 25,
        marginVertical: 10,
        width:300,
        height:300,
        backgroundColor: "#EFEFEF"
    },
    portofolio: {
        flexDirection:"row",
        paddingTop: 20, 
        paddingLeft: 10,
        justifyContent:"Space-around"
    },
    contact: {
        flexDirection:"column",
        paddingTop: 20, 
        paddingLeft: 10,
        alignContent:"center",
        justifyContent:"center"
    },
    title:{
        alignContent: "flex-start", 
        paddingLeft: 20, 
        paddingTop: 15, 
        fontSize: 20
    },
    textItem:{paddingLeft:10, justifyContent:"center", alignSelf:"center", fontSize: 20}
});