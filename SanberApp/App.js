import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tugas from './Tugas/Tugas12/App';
import Login from './Tugas/tugas13/LoginScreen';
import About from './Tugas/tugas13/AboutScreen';
import Main from './Tugas/tugas14/components/Main';
//import Index from './Tugas/tugas15/Index';
import Index from './Quiz3/Index';

export default function App() {
  return (
    //<Tugas></Tugas> //Tugas 12
    //<Login></Login> //Tugas 13
    //<About></About> //Tugas 13
    //<Main></Main> //Tugas 14
    <Index></Index>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
