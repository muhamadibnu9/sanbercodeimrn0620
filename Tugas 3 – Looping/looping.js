var flag = 2;
console.log('LOOPING PERTAMA')
while(flag <= 20) { 
    console.log(flag + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
    flag = flag+2; // Mengubah nilai flag dengan menambahkan 2
}
var flag = 20;
console.log('LOOPING KEDUA')
while(flag >= 2) { 
    console.log(flag + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
    flag = flag-2; // Mengubah nilai flag dengan menambahkan 2
}


//LOOPING FOR
console.log('OUTPUT LOOPING FOR')
for(var angka = 1; angka <= 20; angka++) {
    if (angka%2 == 1) {
        if (angka%3==0){
            console.log(angka + ' - I Love Coding');
        }else{
            console.log(angka + ' - Santai');
        }
    }else{
        console.log(angka + ' - Berkualitas');
    }
    
  } 
  
  console.log('Persegi Panjang')
  panjang = 8;
  lebar = 4;
  for( var l=1; l<=lebar; l++ ){
    pager = "";
      for( var p=1; p<=panjang; p++){
        pager = pager + "#";
      }
      console.log(pager);
      
  }
  console.log('Tangga')
  for( var y=1; y<=7; y++ ){
    pager = "";
      for( var x=1; x<=y; x++){
        pager = pager + "#";
      }
      console.log(pager);
      
  }
  console.log('Catur')
  var ukuran = 8;
  for( var y=1; y<=ukuran; y++ ){
    pager = "";
        if (y%2 == 1){
            for( var x=1; x<=ukuran; x++){
                if (x%2 == 0){
                    pager = pager + "#";
                }else{
                    pager = pager + " ";
                }
            }
        }else{
            for( var x=1; x<=ukuran; x++){
                if (x%2 == 1){
                    pager = pager + "#";
                }else{
                    pager = pager + " ";
                }
              }
        }
      
      console.log(pager);
      
  }