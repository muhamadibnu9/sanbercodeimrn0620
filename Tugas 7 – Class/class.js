console.log("--------------------------")
console.log("       Jawaban 1          ")
console.log("--------------------------")

class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


class Ape extends Animal{
    constructor(name){
        super();
        this.legs = 2;
    }

    yell(){
        //return 'Auooo';
        console.log('Auooo')
    }
}

class Frog extends Animal{
    constructor(name){
        super();
    }

        jump(){
            //return 'hop hop';
            console.log('hop hop')

        }
    
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("--------------------------")
console.log("       Jawaban 2          ")
console.log("--------------------------")



// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
//   }
  
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 

class Clock {
    constructor({template}){
        var date = new Date();
        
        this.hours = date.getHours();
        if (this.hours < 10) this.hours =  this.hours;
        this.mins = date.getMinutes();
        if (this.hours < 10) this.hours = '0' + this.hours;
        this.secs = date.getSeconds();
        if (this.secs < 10) this.secs = '0' + this.secs;
        this.timer = 0;
        this.template = template;
        //this.a = 1;
        var output = template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);
  
      console.log(output);
    
        
    }

    render(){
        var t = this.template;
        console.log(t);
        return new Clock({template: t});
        
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(this.render, 1000);
    }
}

var clock = new Clock({template: 's:m:h'});
clock.start();  