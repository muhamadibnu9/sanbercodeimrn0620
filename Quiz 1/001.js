function bandingkan(num1, num2) {
    // code di sini
    if ((num1 < 0) || (num2 < 0) || (num1 == num2)){
        hasil = -1
    }else{
        if (num1<num2){
            hasil = num2
        }else{
            hasil = num1
        }
    }
    return hasil;
  }
  
  function balikString(kata) {
    // Silakan tulis code kamu di sini
    var x = kata.length;
    var balik = "";
    for (i=x-1;i>=0;i--){
        balik = balik + kata[i];
    }
    return balik;
  }
  
  function palindrome(kata) {
    // Silakan tulis code kamu di sini
    var x = kata.length;
    var balik = "";
    for (i=x-1;i>=0;i--){
        balik = balik + kata[i];
    }
    return (balik == kata);
  }
  
  // TEST CASES Bandingkan Angka
  console.log("------------------------------------")
  console.log("      TEST CASES Bandingkan Angka   ")
  console.log("------------------------------------")
  console.log(bandingkan(10, 15)); // 15
  console.log(bandingkan(12, 12)); // -1
  console.log(bandingkan(-1, 10)); // -1 
  console.log(bandingkan(112, 121));// 121
  console.log(bandingkan(1)); // 1
  console.log(bandingkan()); // -1
  console.log(bandingkan("15", "18")) // 18
  
  // TEST CASES BalikString
  console.log("------------------------------------")
  console.log("      TEST CASES BalikString        ")
  console.log("------------------------------------")
  console.log(balikString("abcde")) // edcba
  console.log(balikString("rusak")) // kasur
  console.log(balikString("racecar")) // racecar
  console.log(balikString("haji")) // ijah
  
  // TEST CASES Palindrome
  console.log("------------------------------------")
  console.log("      TEST CASES Palindrome         ")
  console.log("------------------------------------")
  console.log(palindrome("kasur rusak")) // true
  console.log(palindrome("haji ijah")) // true
  console.log(palindrome("nabasan")) // false
  console.log(palindrome("nababan")) // true
  console.log(palindrome("jakarta")) // false