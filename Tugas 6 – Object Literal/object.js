console.log("--------------------------")
console.log("       Jawaban 1          ")
console.log("--------------------------")
//SOAL 1
function arrayToObject(arr) {
    var lengtArray = arr.length;
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    for (x = 0;x<lengtArray;x++){
        if (arr[x][3] == undefined || (thisYear < arr[x][3])){
            agev = 'Invalid Birth Day';
        }else{
            agev = thisYear - arr[x][3]
        }
        
        var person= { 
            firstName: arr[x][0],
            lastName: arr[x][1],
            gender: arr[x][2],
            age: agev
        }
        var myJSON = JSON.stringify(person);
        console.log(`${x+1}. ${person.firstName} ${person.lastName} : ${myJSON}`);
        
    }
    if (lengtArray == 0){
        console.log(`""`);
    }

}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("--------------------------")
console.log("       Jawaban 2          ")
console.log("--------------------------")
//soal 2
function shoppingTime(memberId, money) {
    pesanError1 = 'Mohon maaf, toko X hanya berlaku untuk member saja';
    pesanError2 = 'Mohon maaf, uang tidak cukup';
    var member = {};
    if (memberId == '' || memberId == undefined){
        return pesanError1;
    } else if (money < 50000){
        return pesanError2;
    } else {
        member.memberId = memberId;
        member.money = money;
        arrLi = [];
        var ss = 0;
        var bz = 0;
        var su = 0;
        var bh = 0;
        var ch = 0;
        while(money >= 50000 && ch == 0){
            switch (true) {
                case (money >= 1500000 && ss == 0) : {
                    arrLi.push('Sepatu Stacattu');
                    money = money - 1500000;
                    ss = 1;
                    break;
                }
                    
                case (money >= 500000 && bz == 0) :{
                    arrLi.push('Baju Zoro');
                    money = money - 500000;
                    bz = 1;
                    break;
                }
                case (money >= 250000 && su == 0) : {
                    arrLi.push('Baju H&N');
                    money = money - 250000;
                    su = 1;
                    break;
                }
                case (money >= 175000 &&  bh == 0) : {
                    arrLi.push('Sweater Uniklooh');
                    money = money - 175000;
                    bh = 1;
                    break;
                }
                case (money >= 50000 && ch == 0) : {
                    arrLi.push('Casing Handphone');
                    money = money - 50000;
                    ch = 1;
                    break;
                }
            } 
               
            
        }
            
        
        member.listPurchased = arrLi;
        member.changeMoney = money;
        return member;
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("--------------------------")
console.log("       Jawaban 3          ")
console.log("--------------------------")
//soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    sRute = rute.join('');
    listpenumpang = {};
    arrlength= arrPenumpang.length;
    function Penumpang(nama,dari,tujuan,tarif){
        this.nama = nama;
        this.naikDari = dari;
        this.tujuan = tujuan;
        this.bayar = tarif;
    }

    if (arrlength > 0) {
        for (x=0; x<arrlength; x++){
            if(arrPenumpang[x][0] != undefined && arrPenumpang[x][1] != undefined && arrPenumpang[x][2] != undefined ){
                startRute =sRute.indexOf(arrPenumpang[x][1]);
                endRute = sRute.indexOf(arrPenumpang[x][2]);
                tarif = 2000*(endRute-startRute);
                

                listpenumpang[x] = new Penumpang (arrPenumpang[x][0], arrPenumpang[x][1], arrPenumpang[x][2], tarif);
            }
        }
    }else{
        listpenumpang = [];
        
    }
    return listpenumpang;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]