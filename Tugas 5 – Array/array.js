console.log("--------------------------")
console.log("       Jawaban 1          ")
console.log("--------------------------")
//Soal No. 1 (Range)
function range(startNum,endNum)
{
    var arrPen = [];
    if (startNum > endNum) {
        for (x=startNum;x>=endNum;x--){
            arrPen.push(x);
        }
    }else if (startNum < endNum){
        for (x=startNum;x<=endNum;x++){
            arrPen.push(x);
        }
    }else{
        arrPen = [-1];
    }

    return arrPen;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("--------------------------")
console.log("       Jawaban 2          ")
console.log("--------------------------")
//SOAL 2

function rangeWithStep(startNum,endNum,step)
{
    var arrPen = [];
    if (startNum > endNum) {
        for (x=startNum;x>=endNum;x=x-step){
            arrPen.push(x);
        }
    }else if (startNum < endNum){
        for (x=startNum;x<=endNum;x=x+step){
            arrPen.push(x);
        }
    }else{
        arrPen = [-1];
    }

    return arrPen;
}
function rangeWithStep(startNum,endNum,step)
{
    var arrPen = [];
    if (startNum > endNum) {
        for (x=startNum;x>=endNum;x=x-step){
            arrPen.push(x);
        }
    }else if (startNum < endNum){
        for (x=startNum;x<=endNum;x=x+step){
            arrPen.push(x);
        }
    }else{
        arrPen = [-1];
    }

    return arrPen;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("--------------------------")
console.log("       Jawaban 3          ")
console.log("--------------------------")
//Soal 3

function sum(startNum,endNum,step)
{
    if (step == undefined){
        step = 1;
    }
    if (endNum == undefined){
        endNum = 1;
    }
    var jumArr = 0;
    if (startNum > endNum) {
        for (x=startNum;x>=endNum;x=x-step){
            jumArr = jumArr + x;
        }
    }else if (startNum <= endNum){
        for (x=startNum;x<=endNum;x=x+step){
            jumArr = jumArr + x;
        }
    }else{
        jumArr = -1;
    }

    return jumArr;
    
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("--------------------------")
console.log("       Jawaban 4          ")
console.log("--------------------------")
//Soal 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(dataArr){
    var i = dataArr.length;
    for (x=0;x<i;x++){
        console.log("Nomor ID:  "+dataArr[x][0])
        console.log("Nama Lengkap:  "+dataArr[x][1])
        console.log("TTL:  "+dataArr[x][2]+" "+dataArr[x][3])
        console.log("Hobby:  "+dataArr[x][4])
        console.log("                              ")
    }
}
dataHandling(input);

console.log("--------------------------")
console.log("       Jawaban 5          ")
console.log("--------------------------")
//Soal 5

function balikKata(kata){
    var x = kata.length;
    var balik = "";
    for (i=x-1;i>=0;i--){
        balik = balik + kata[i];
    }
    return balik;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("--------------------------")
console.log("       Jawaban 6          ")
console.log("--------------------------")
//Soal 6
function dataHandling2(dataArr2){
   // dataArr2[1] = dataArr2[1] +"Elsharawy";
    var kataB = dataArr2[1].split(" ");
    kataB.pop();
    kataB.push("Elsharawy")
    dataArr2[1] = kataB.join(" ")
    dataArr2[2] = "Provinsi "+dataArr2[2];
    dataArr2.splice(4, 1, "Pria", "SMA Internasional Metro")
    var ttl = dataArr2[3].split("/")
    namabulan = cekbulan(Number(ttl[1]))
    ttl2 = ttl.join("-");
    ttl.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(dataArr2);
    console.log(namabulan);
    console.log(ttl);
    console.log(ttl2);
    console.log(dataArr2[1].slice(0,15));
}
function cekbulan(nobulan){
    switch(nobulan) {
        case 1:   { strBulan = "Januari"; break; }
        case 2:   { strBulan = "Februari"; break; }
        case 3:   { strBulan = "Maret"; break; }
        case 4:   { strBulan = "April"; break; }
        case 5:   { strBulan = "Mei"; break; }
        case 6:   { strBulan = "Juni"; break; }
        case 7:   { strBulan = "Juli"; break; }
        case 8:   { strBulan = "Agustus"; break; }
        case 9:   { strBulan = "September"; break; }
        case 10:   { strBulan = "Oktober"; break; }
        case 11:   { strBulan = "November"; break; }
        case 12:   { strBulan = "Desember"; break; }
        default:  { strBulan = "Input Bulan anda salah!"; break;}
        
    }
    return strBulan;
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 