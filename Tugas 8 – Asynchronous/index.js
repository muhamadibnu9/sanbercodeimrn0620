// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var x = 0;
var time = 10000
function readTime(x) {
    
    if(x != books.length){
        readBooks(time, books[x], (sisa) => {
            time = sisa,
            readTime(x=x+1);
        })
    }
    else{
        console.log('Saya sudah membaca')
        return 0
    }
}
readTime(0)